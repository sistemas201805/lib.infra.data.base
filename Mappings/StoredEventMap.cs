using Lib.Domain.Core.Messages.Events;
using Lib.Infra.Data.Base.Extensions;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Lib.Infra.Data.Base.Mappings
{
    public class StoredEventMap : EntityTypeConfiguration<StoredEvent>
    {
        public override void Map(EntityTypeBuilder<StoredEvent> builder)
        {
            builder.Property(c => c.DataCriacao);

            builder.Property(c => c.Acao)
                .HasMaxLength(100);

            builder.Property(c => c.Data)
                .HasMaxLength(1000);

            builder.Property(c => c.User)
                .HasMaxLength(255);
        }
    }
}
