using Lib.Domain.Core.Messages.Events;
using Lib.Infra.Data.Base.Extensions;
using Lib.Infra.Data.Base.Mappings;
using Microsoft.EntityFrameworkCore;

namespace Lib.Infra.Data.Base.Context
{
    public abstract class BaseDbContext : DbContext
    {
        public string EnvironmentName;
        public DbSet<StoredEvent> StoredEvent { get; set; }

        public BaseDbContext() { }
        public BaseDbContext(DbContextOptions options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddConfiguration(new StoredEventMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
