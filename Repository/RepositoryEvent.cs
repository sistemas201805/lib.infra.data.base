using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Lib.Domain.Core.Interfaces;
using Lib.Domain.Core.Messages.Events;
using Microsoft.EntityFrameworkCore;

namespace Lib.Infra.Data.Base.Repository
{
    public abstract class RepositoryEvent<TEntity> : IRepositoryEvent<TEntity> where TEntity : Event
    {
        protected DbContext Db;
        protected DbSet<TEntity> DbSet;
        protected RepositoryEvent(DbContext context)
        {
            Db = context;
            DbSet = Db.Set<TEntity>();
        }

        public virtual void Adicionar(TEntity obj)
        {
            DbSet.Add(obj);
        }

        public virtual void Atualizar(TEntity obj)
        {
            DbSet.Update(obj);
        }

        public virtual IEnumerable<TEntity> Buscar(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.AsNoTracking().Where(predicate);
        }

        public virtual TEntity ObterPorId(Guid id)
        {
            return DbSet.AsNoTracking().FirstOrDefault(t => t.Id == id);
        }

        public virtual IEnumerable<TEntity> ObterTodos()
        {
            return DbSet.ToList();
        }

        public virtual void Excluir(Guid id)
        {
            DbSet.Remove(DbSet.Find(id));
        }

        public int SaveChanges()
        {
            return Db.SaveChanges();
        }

        public virtual void Dispose()
        {
            Db.Dispose();
        }
    }
}
