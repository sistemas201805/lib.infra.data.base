using Lib.Domain.Core.Interfaces;
using Lib.Domain.Core.Messages.Events;
using Lib.Infra.Data.Base.Repository.EventSourcing;
using Newtonsoft.Json;

namespace Lib.Infra.Data.Base.EventSourcing
{
    public class StoredEventController : IStoredEventController
    {
        private readonly IStoredEventRepository _storedEventRepository;
        private readonly IUser _user;

        public StoredEventController(IStoredEventRepository storedEventRepository, IUser user)
        {
            _storedEventRepository = storedEventRepository;
            _user = user;
        }

        public void SalvarEvento<T>(T evento) where T : Event
        {
            var serializedData = JsonConvert.SerializeObject(evento);

            var storedEvent = new StoredEvent(
                evento,
                serializedData,
                _user.GetUserId().ToString());

            _storedEventRepository.Adicionar(storedEvent);
        }
    }
}
