using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Lib.Infra.Data.Base.Extensions
{
    public abstract class EntityTypeConfiguration<TEntity> where TEntity : class
    {
        public abstract void Map(EntityTypeBuilder<TEntity> builder);
    }
}
